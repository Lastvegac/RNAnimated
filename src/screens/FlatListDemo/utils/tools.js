import TouchID from 'react-native-touch-id';
import {optionalConfigObject} from './constant';
import {Alert} from 'react-native';

export const pressHandlerTouchId = () => {
  TouchID.authenticate('demo react-native-touchId', optionalConfigObject)
    .then(success => {
      return Alert.alert('Authenticated Successfully', success);
    })
    .catch(error => {
      return Alert.alert('Authentication Failed', error);
    });
};

export const getMusicNoteAnim = (animatedValue, isRotatedLeft) => {
  return {
    transform: [
      {
        translateX: animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [8, -16],
        }),
      },
      {
        translateY: animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [0, -32],
        }),
      },
      {
        rotate: animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: ['0deg', isRotatedLeft ? '-45deg' : '45deg'],
        }),
      },
    ],
    opacity: animatedValue.interpolate({
      inputRange: [0, 0.8, 1],
      outputRange: [0, 1, 0],
    }),
  };
};
