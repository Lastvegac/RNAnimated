import {Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export const SCREEN_WIDTH = width;
export const SCREEN_HEIGHT = height;

export const optionalConfigObject = {
  title: 'Authentication Required', // Android
  imageColor: '#e00606', // Android
  imageErrorColor: '#ff0000', // Android
  sensorDescription: 'Touch sensor', // Android
  sensorErrorDescription: 'Failed', // Android
  cancelText: 'Cancel', // Android
  fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
  unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export const optionalConfigIsSupported = {
  unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false, // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
};

export const DATA_USER = [
  {
    id: 1,
    name: 'Bello Phill',
    country: 'Quang Nam',
    avatar:
      'https://cdn1.vectorstock.com/i/1000x1000/31/95/user-sign-icon-person-symbol-human-avatar-vector-12693195.jpg',
    age: '27',
    type: 'React Native',
  },
  {
    id: 2,
    name: 'Suu Pham',
    country: 'Quang Ngai',
    avatar:
      'https://banner2.cleanpng.com/20180626/fhs/kisspng-avatar-user-computer-icons-software-developer-5b327cc98b5780.5684824215300354015708.jpg',
    age: '28',
    type: 'React JS',
  },
  {
    id: 3,
    name: 'Thuan Cao',
    country: 'Quang Tri',
    avatar:
      'https://banner2.cleanpng.com/20180625/req/kisspng-computer-icons-avatar-business-computer-software-user-avatar-5b3097fcae25c3.3909949015299112927133.jpg',
    age: '30',
    type: 'Swift',
  },
  {
    id: 4,
    name: 'Van Lee',
    country: 'Quang Binh',
    avatar:
      'https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png',
    age: '26',
    type: 'Flutter',
  },
  {
    id: 5,
    name: 'Tao Ho',
    country: 'Quang Nam',
    avatar:
      'https://cdn1.vectorstock.com/i/1000x1000/31/95/user-sign-icon-person-symbol-human-avatar-vector-12693195.jpg',
    age: '27',
    type: 'React Native',
  },
  {
    id: 6,
    name: 'Sy Nguyen',
    country: 'Quang Ngai',
    avatar:
      'https://banner2.cleanpng.com/20180626/fhs/kisspng-avatar-user-computer-icons-software-developer-5b327cc98b5780.5684824215300354015708.jpg',
    age: '28',
    type: 'React JS',
  },
  {
    id: 7,
    name: 'Manh Nguyen',
    country: 'Quang Tri',
    avatar:
      'https://banner2.cleanpng.com/20180625/req/kisspng-computer-icons-avatar-business-computer-software-user-avatar-5b3097fcae25c3.3909949015299112927133.jpg',
    age: '30',
    type: 'Swift',
  },
  {
    id: 8,
    name: 'Tan Nguyen',
    country: 'Quang Binh',
    avatar:
      'https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png',
    age: '26',
    type: 'Flutter',
  },
  {
    id: 9,
    name: 'Dung Tran',
    country: 'Quang Nam',
    avatar:
      'https://cdn1.vectorstock.com/i/1000x1000/31/95/user-sign-icon-person-symbol-human-avatar-vector-12693195.jpg',
    age: '27',
    type: 'React Native',
  },
  {
    id: 10,
    name: 'Duc Le',
    country: 'Quang Ngai',
    avatar:
      'https://banner2.cleanpng.com/20180626/fhs/kisspng-avatar-user-computer-icons-software-developer-5b327cc98b5780.5684824215300354015708.jpg',
    age: '28',
    type: 'React JS',
  },
  {
    id: 11,
    name: 'Thuan Le',
    country: 'Quang Tri',
    avatar:
      'https://banner2.cleanpng.com/20180625/req/kisspng-computer-icons-avatar-business-computer-software-user-avatar-5b3097fcae25c3.3909949015299112927133.jpg',
    age: '30',
    type: 'Swift',
  },
  {
    id: 12,
    name: 'Quang Le',
    country: 'Quang Binh',
    avatar:
      'https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png',
    age: '26',
    type: 'Flutter',
  },
  {
    id: 13,
    name: 'Duong Do',
    country: 'Quang Nam',
    avatar:
      'https://cdn1.vectorstock.com/i/1000x1000/31/95/user-sign-icon-person-symbol-human-avatar-vector-12693195.jpg',
    age: '27',
    type: 'React Native',
  },
  {
    id: 14,
    name: 'Meo Yeu',
    country: 'Quang Ngai',
    avatar:
      'https://banner2.cleanpng.com/20180626/fhs/kisspng-avatar-user-computer-icons-software-developer-5b327cc98b5780.5684824215300354015708.jpg',
    age: '28',
    type: 'React JS',
  },
  {
    id: 15,
    name: 'Thanh Tran',
    country: 'Quang Tri',
    avatar:
      'https://banner2.cleanpng.com/20180625/req/kisspng-computer-icons-avatar-business-computer-software-user-avatar-5b3097fcae25c3.3909949015299112927133.jpg',
    age: '30',
    type: 'Swift',
  },
  {
    id: 16,
    name: 'Phu Le',
    country: 'Quang Binh',
    avatar:
      'https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png',
    age: '26',
    type: 'Flutter',
  },
];

export const IMG_EMPTY_DATA =
  'https://cdn.dribbble.com/users/1377014/screenshots/4287624/empty_list.png';

export const IMG_HEADER_LIST =
  'https://hips.hearstapps.com/wdy.h-cdn.co/assets/17/39/1506709524-cola-0247.jpg?crop=1.00xw:0.750xh;0,0.226xh&resize=480:*';

export const IMG_FOOTER_LIST =
  'https://t3.ftcdn.net/jpg/02/07/57/40/360_F_207574038_Tdu7qC09YHyN56ZmcDWIvfqsrniUiyI2.jpg';

export const ITEM_HEIGHT = 100;

export const viewabilityConfig = {
  minimumViewTime: 700, // Khoảng thời gian tối thiểu (tính bằng mili giây) mà item phải có thể xem thực tế trước khi lệnh gọi lại khả năng xem được kích hoạt.
  // Con số càng cao có nghĩa là cuộn qua nội dung mà không dừng lại sẽ không đánh dấu nội dung là có thể xem được.

  waitForInteraction: true, //  khi user  bắt đầu scroll thì mới bắt đầu thực hiện tính viewable.

  // At least one of the viewAreaCoveragePercentThreshold or itemVisiblePercentThreshold is required.
  viewAreaCoveragePercentThreshold: 95, // có giá trị từ 0-100 (%), khi item show được  bao nhiêu % thì nó sẽ thực thi các hàm được chỉ định .

  // itemVisiblePercentThreshold: 75, // Tương tự như viewAreaCoveragePercentThreshold,
  //nhưng xem xét phần trăm của mục được hiển thị, thay vì phần của khu vực có thể xem mà nó bao phủ.
};

export const videosData = [
  {
    id: 1,
    channelName: 'cutedog',
    uri: 'https://v.pinimg.com/videos/mc/720p/f6/88/88/f68888290d70aca3cbd4ad9cd3aa732f.mp4',
    caption: 'Cute dog shaking hands #cute #puppy',
    musicName: 'Song #1',
    likes: 4321,
    comments: 2841,
    avatarUri: 'https://wallpaperaccess.com/full/1669289.jpg',
  },
  {
    id: 2,
    channelName: 'meow',
    uri: 'https://v.pinimg.com/videos/mc/720p/11/05/2c/11052c35282355459147eabe31cf3c75.mp4',
    caption: 'Doggies eating candy #cute #puppy',
    musicName: 'Song #2',
    likes: 2411,
    comments: 1222,
    avatarUri: 'https://wallpaperaccess.com/thumb/266770.jpg',
  },
  {
    id: 3,
    channelName: 'yummy',
    uri: 'https://v.pinimg.com/videos/mc/720p/c9/22/d8/c922d8391146cc2fdbeb367e8da0d61f.mp4',
    caption: 'Brown little puppy #cute #puppy',
    musicName: 'Song #3',
    likes: 3100,
    comments: 801,
    avatarUri: 'https://wallpaperaccess.com/thumb/384178.jpg',
  },
  {
    id: 4,
    channelName: 'cutedog',
    uri: 'https://v.pinimg.com/videos/mc/720p/f6/88/88/f68888290d70aca3cbd4ad9cd3aa732f.mp4',
    caption: 'Cute dog shaking hands #cute #puppy',
    musicName: 'Song #1',
    likes: 4321,
    comments: 2841,
    avatarUri: 'https://wallpaperaccess.com/full/1669289.jpg',
  },
  {
    id: 5,
    channelName: 'meow',
    uri: 'https://v.pinimg.com/videos/mc/720p/11/05/2c/11052c35282355459147eabe31cf3c75.mp4',
    caption: 'Doggies eating candy #cute #puppy',
    musicName: 'Song #2',
    likes: 2411,
    comments: 1222,
    avatarUri: 'https://wallpaperaccess.com/thumb/266770.jpg',
  },
  {
    id: 6,
    channelName: 'yummy',
    uri: 'https://v.pinimg.com/videos/mc/720p/c9/22/d8/c922d8391146cc2fdbeb367e8da0d61f.mp4',
    caption: 'Brown little puppy #cute #puppy',
    musicName: 'Song #3',
    likes: 3100,
    comments: 801,
    avatarUri: 'https://wallpaperaccess.com/thumb/384178.jpg',
  },
];
