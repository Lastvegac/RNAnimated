import React, {useCallback, memo, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  RefreshControl,
  View,
  Text,
  TouchableOpacity,
  Platform,
} from 'react-native';
import EmptyData from './component/EmptyData';
import FooterListComponent from './component/FooterListComponent';
import HeaderListComponent from './component/HeaderListComponent';
import ItemUser from './component/ItemUser';
import {
  DATA_USER,
  ITEM_HEIGHT,
  SCREEN_WIDTH,
  viewabilityConfig,
} from './utils/constant';

const RenderItem = memo(({item, index}) => {
  // console.log('vào đây không?');
  return <ItemUser item={item} index={index} />;
});

const TouchIdScreen = () => {
  const [count, setCount] = useState(0);

  const onRefresh = () => {};

  const renderCount = () => {
    return (
      <View>
        <Text style={styles.txtCount}>{count}</Text>
        <View style={styles.rows}>
          <TouchableOpacity style={styles.btnCount} onPress={handleCountAsc}>
            <Text style={styles.txtIncreasing}>ASC</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnCount} onPress={handleCountDesc}>
            <Text style={styles.txtIncreasing}>DESC</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderItem = useCallback(({item, index}) => {
    return <ItemUser item={item} index={index} />;
  }, []);

  const renderEmptyComponent = () => {
    return <EmptyData />;
  };

  const renderHeaderComponent = () => {
    return <HeaderListComponent />;
  };

  const renderFooterComponent = () => {
    return <FooterListComponent />;
  };

  const getItemLayout = useCallback(
    (_data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    }),
    [],
  );

  const keyExtractor = useCallback(item => `${item.id}`, []);

  const handleCountAsc = () => {
    setCount(pre => pre + 1);
  };

  const handleCountDesc = () => {
    setCount(pre => pre - 1);
  };

  const onViewableItemsChanged = ({viewableItems, changed}) => {
    console.log('viewableItems', viewableItems);
    console.log('changed', changed);
    //viewableItems : đã show, isViewable: true
    //changed: đã load rồi, mà chưa nhìn thấy trên view // isViewable: false
  };

  const handleItemsPartiallyVisible100 = ({viewableItems, changed}) => {
    // console.log('viewableItems 100', viewableItems, changed);
  };

  const handleItemsPartiallyVisible10 = ({viewableItems, changed}) => {
    // console.log('viewableItems 10', viewableItems, changed);
  };

  const viewabilityConfigCallbackPairs = [
    {
      viewabilityConfig: {
        minimumViewTime: 500,
        itemVisiblePercentThreshold: 100,
      },
      onViewableItemsChanged: handleItemsPartiallyVisible100,
    },
    {
      viewabilityConfig: {
        minimumViewTime: 150,
        itemVisiblePercentThreshold: 10,
      },
      onViewableItemsChanged: handleItemsPartiallyVisible10,
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {/* {renderCount()} */}
        <FlatList
          style={styles.fill}
          showsVerticalScrollIndicator={false}
          data={DATA_USER || []}
          renderItem={renderItem}
          // renderItem={({item, index}) => (
          //   <RenderItem item={item} index={index} />
          // )}
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={onRefresh}
              tintColor={'green'}
              colors={['green', 'red']}
            />
          }
          keyExtractor={keyExtractor}
          ListEmptyComponent={renderEmptyComponent}
          ListHeaderComponent={renderHeaderComponent}
          ListFooterComponent={renderFooterComponent}
          maxToRenderPerBatch={5}
          initialNumToRender={10}
          windowSize={21}
          getItemLayout={getItemLayout}
          removeClippedSubviews={Platform.OS === 'android' ? true : false}
          extraData={DATA_USER}
          viewabilityConfig={viewabilityConfig}
          onViewableItemsChanged={onViewableItemsChanged}
          stickyHeaderHiddenOnScroll={true}
          stickyHeaderIndices={[0]} // 0 => for first item, 1 for second item, ... // can use [x,y,z] // not support horizontal
          // viewabilityConfigCallbackPairs={viewabilityConfigCallbackPairs}
        />
      </View>
    </View>
  );
};

export default TouchIdScreen;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnTouchId: {
    backgroundColor: 'green',
    borderRadius: 8,
  },
  txtAuthenticate: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    color: '#fff',
    fontWeight: 'bold',
  },
  viewUser: {
    flex: 1,
    borderWidth: 1,
    marginBottom: 10,
    width: '94%',
    height: '100%',
    borderRadius: 5,
    borderColor: 'gray',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    alignSelf: 'center',
    backgroundColor: '#E9F4FF',
  },
  fill: {
    flex: 1,
  },
  imgEmptyData: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH,
  },
  imgAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  txtNameUser: {
    fontWeight: 'bold',
  },
  content: {
    width: '100%',
    height: '100%',
    marginTop: 30,
  },
  imgHeader: {
    width: SCREEN_WIDTH - 26,
    height: 180,
    marginBottom: 10,
    borderRadius: 5,
    alignSelf: 'center',
  },
  imgFooter: {
    width: SCREEN_WIDTH - 50,
    height: 300,
    marginBottom: 10,
    borderRadius: 5,
    alignSelf: 'center',
  },
  btnCount: {
    width: SCREEN_WIDTH / 3,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
    borderRadius: 8,
    alignSelf: 'center',
    marginBottom: 20,
  },
  rows: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '80%',
    alignSelf: 'center',
  },
  txtIncreasing: {
    color: 'white',
    fontWeight: 'bold',
  },
  txtCount: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 30,
    marginBottom: 30,
  },
});
