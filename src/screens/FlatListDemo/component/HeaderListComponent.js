import React from 'react';
import FastImage from 'react-native-fast-image';
import {styles} from '../FlatlistScreen';
import {IMG_HEADER_LIST} from '../utils/constant';

const HeaderListComponent = () => {
  return (
    <FastImage
      style={styles.imgHeader}
      source={{
        uri: IMG_HEADER_LIST,
      }}
    />
  );
};

export default HeaderListComponent;
