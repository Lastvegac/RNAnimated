import {Text, View} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {styles} from '../FlatlistScreen';

const ItemUser = ({item, index = 0}) => {
  const {name, age, type, avatar, country} = item || {};
  return (
    <View key={index} style={styles.viewUser}>
      <FastImage
        style={styles.imgAvatar}
        source={{
          uri: avatar,
          priority: FastImage.priority.normal,
        }}
      />
      <View>
        <Text style={styles.txtNameUser}>N: {name}</Text>
        <Text style={styles.txtNameUser}>C: {country}</Text>
        <Text style={styles.txtNameUser}>A: {age}</Text>
        <Text style={styles.txtNameUser}>T: {type}</Text>
      </View>
    </View>
  );
};

export default ItemUser;
