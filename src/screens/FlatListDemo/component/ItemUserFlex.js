import {Text, View, StyleSheet} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {SCREEN_HEIGHT} from '../utils/constant';

const ItemUserFlex = ({item, index = 0}) => {
  const {name, age, type, avatar, country} = item || {};
  return (
    <View key={index} style={styles.viewUser}>
      <FastImage
        style={styles.imgAvatar}
        source={{
          uri: avatar,
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View>
        <Text style={styles.txtNameUser}>N: {name}</Text>
        <Text style={styles.txtNameUser}>C: {country}</Text>
        <Text style={styles.txtNameUser}>A: {age}</Text>
        <Text style={styles.txtNameUser}>T: {type}</Text>
      </View>
    </View>
  );
};

export default ItemUserFlex;

const styles = StyleSheet.create({
  viewUser: {
    backgroundColor: 'green',
    width: '98%',
    height: SCREEN_HEIGHT,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'white',
  },
  imgAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  txtNameUser: {
    fontWeight: 'bold',
    color: 'white',
  },
});
