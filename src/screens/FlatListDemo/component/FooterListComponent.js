import React from 'react';
import FastImage from 'react-native-fast-image';
import {styles} from '../FlatlistScreen';
import {IMG_FOOTER_LIST} from '../utils/constant';

const FooterListComponent = () => {
  return (
    <FastImage
      style={styles.imgFooter}
      source={{
        uri: IMG_FOOTER_LIST,
      }}
    />
  );
};

export default FooterListComponent;
