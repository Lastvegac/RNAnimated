import {View} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {styles} from '../FlatlistScreen';
import {IMG_EMPTY_DATA} from '../utils/constant';

const EmptyData = () => {
  return (
    <View style={styles.fill}>
      <FastImage
        style={styles.imgEmptyData}
        source={{
          uri: IMG_EMPTY_DATA,
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />
    </View>
  );
};

export default EmptyData;
